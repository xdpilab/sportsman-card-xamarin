﻿using Xamarin.Forms;

namespace SportsmenCard
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new EnterNumberPage());

            //MainPage = new NavigationPage(new EnterNumberPage())
            //{
            //    BarBackgroundColor = Color.FromHex("#f4f4f6"),
            //    BarTextColor = Color.Red,
            //};
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }

}