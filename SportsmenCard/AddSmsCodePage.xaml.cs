﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SportsmenCard
{
    public partial class AddSmsCodePage : ContentPage
    {
        public AddSmsCodePage()
        {
            InitializeComponent();

            // set platform dependent logic
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    // hide navigation bar
                    NavigationPage.SetHasNavigationBar(this, false);
                    break;
            }

            // set listeners
            tv1.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel1Clicked()),
            });
            tv2.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel2Clicked()),
            });
            tv3.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel3Clicked()),
            });
            tv4.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel4Clicked()),
            });
            tv5.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel5Clicked()),
            });
            tv6.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel6Clicked()),
            });
            tv7.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel7Clicked()),
            });
            tv8.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel8Clicked()),
            });
            tv9.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel9Clicked()),
            });
            tv0.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabel0Clicked()),
            });
            tvBack.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabelBackClicked()),
            });
            tvSendSmsAgain.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => OnLabelSendSmsAgainClicked()),
            });

        }

        private void OnLabel1Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "1 ";
        }
        private void OnLabel2Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "2 ";
        }
        private void OnLabel3Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "3 ";
        }
        private void OnLabel4Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "4 ";
        }
        private void OnLabel5Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "5 ";
        }
        private void OnLabel6Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "6 ";
        }
        private void OnLabel7Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "7 ";
        }
        private void OnLabel8Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "8 ";
        }
        private void OnLabel9Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "9 ";
        }
        private void OnLabel0Clicked()
        {
            tvSmsCode.Text = tvSmsCode.Text + "0 ";

        }
        private void OnLabelBackClicked()
        {
            if (!tvSmsCode.Text.Equals(" "))
                tvSmsCode.Text = tvSmsCode.Text.Remove(tvSmsCode.Text.Length - 2);
        }
        private void OnLabelSendSmsAgainClicked()
        {
            Navigation.PushAsync(new RegistrationPage());

        }
    }

}