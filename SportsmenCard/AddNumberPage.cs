﻿using System;

using Xamarin.Forms;

namespace SportsmenCard
{
    public class AddNumberPage : ContentPage
    {
        public AddNumberPage()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };

            Content.VerticalOptions = LayoutOptions.Center;
        }
    }
}

