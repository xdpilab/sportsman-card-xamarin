﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SportsmenCard
{
    public partial class RegistrationPage : ContentPage
    {
        public RegistrationPage()
        {
            InitializeComponent();

            // set platform dependent logic
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    // hide navigation bar
                    NavigationPage.SetHasNavigationBar(this, false);

                    etRegion.BackgroundColor = Color.Transparent;
                    etSurname.BackgroundColor = Color.Transparent;
                    etName.BackgroundColor = Color.Transparent;
                    etPatronymic.BackgroundColor = Color.Transparent;
                    //etBirthday.BackgroundColor = Color.Transparent;

                    break;
            }

            separator1.Opacity = 0.2;
            separator2.Opacity = 0.2;
            separator3.Opacity = 0.2;
            separator4.Opacity = 0.2;
            separator5.Opacity = 0.2;
            separator6.Opacity = 0.2;

            separator1.Margin = new Thickness(-20, 0, -20, 0);
            separator2.Margin = new Thickness(-20, 0, -20, 0);
            separator3.Margin = new Thickness(-20, 0, -20, 0);
            separator4.Margin = new Thickness(-20, 0, -20, 0);
            separator5.Margin = new Thickness(-20, 0, -20, 0);
            separator6.Margin = new Thickness(-20, 0, -20, 0);

            btnMan.BackgroundColor = Color.Gray;
            btnMan.TextColor = Color.White;
            btnWoman.TextColor = Color.Gray;
            btnWoman.BackgroundColor = Color.Transparent;

            object name = "";
            if (Application.Current.Properties.TryGetValue("etRegion", out name))
                etRegion.Text = (string)Application.Current.Properties["etRegion"];
            if (Application.Current.Properties.TryGetValue("etSurname", out name))
                etSurname.Text = (string)Application.Current.Properties["etSurname"];
            if (Application.Current.Properties.TryGetValue("etName", out name))
                etName.Text = (string)Application.Current.Properties["etName"];
            if (Application.Current.Properties.TryGetValue("etPatronymic", out name))
                etPatronymic.Text = (string)Application.Current.Properties["etPatronymic"];
            //if (Application.Current.Properties.TryGetValue("etBirthday", out name))
                //etBirthday.Text = (string)Application.Current.Properties["etBirthday"];
        }

        private void btnManClicked(object sender, EventArgs e)
        {
            btnMan.BackgroundColor = Color.Gray;
            btnMan.TextColor = Color.White;
            btnWoman.TextColor = Color.Gray;
            btnWoman.BackgroundColor = Color.Transparent;
        }

        private void btnWomanClicked(object sender, EventArgs e)
        {
            btnWoman.BackgroundColor = Color.Gray;
            btnWoman.TextColor = Color.White;
            btnMan.TextColor = Color.Gray;
            btnMan.BackgroundColor = Color.Transparent;
        }

        private void btnFurtherClicked(object sender, EventArgs e)
        {
            if (Constant.CHECK_FIELDS_REG)
                if (etRegion.Text != null && etSurname.Text != null && etName.Text != null 
                    //&& etBirthday.Text != null
                   )
                {
                    Navigation.PushAsync(new RegistrationListPeoplePage());

                    Application.Current.Properties.Remove("etRegion");
                    Application.Current.Properties.Add("etRegion", etRegion.Text);
                    Application.Current.Properties.Remove("etSurname");
                    Application.Current.Properties.Add("etSurname", etSurname.Text);
                    Application.Current.Properties.Remove("etName");
                    Application.Current.Properties.Add("etName", etName.Text);
                    Application.Current.Properties.Remove("etPatronymic");
                    Application.Current.Properties.Add("etPatronymic", etPatronymic.Text);
                    //Application.Current.Properties.Remove("etBirthday");
                    //Application.Current.Properties.Add("etBirthday", etBirthday.Text);
                }
                else
                    DisplayAlert(null, AppResources.not_all_fields_complete, AppResources.ok);
            else
                Navigation.PushAsync(new RegistrationListPeoplePage());
        }


        private void datePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            //label.Text = "Вы выбрали " + e.NewDate.ToString("dd/MM/yyyy");
        }

    }
}
