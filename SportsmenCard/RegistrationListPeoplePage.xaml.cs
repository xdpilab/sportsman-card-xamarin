﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SportsmenCard
{
    public partial class RegistrationListPeoplePage : ContentPage
    {
        public RegistrationListPeoplePage()
        {
            InitializeComponent();

            // set platform dependent logic
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    // hide navigation bar
                    NavigationPage.SetHasNavigationBar(this, false);
                    break;
            }
        }

        private void btnImNotInlistClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegistrationInfoPage());
        }
    }
}
