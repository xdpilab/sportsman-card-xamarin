﻿using System;
using System.Collections.Generic;
using Plugin.Toasts;
using Xamarin.Forms;

namespace SportsmenCard
{
    public partial class EnterNumberPage : ContentPage
    {
        public EnterNumberPage()
        {
            InitializeComponent();

            // set platform dependent logic
            switch (Device.RuntimePlatform)
            {
                case Device.Android:

                    // hide navigation bar
                    NavigationPage.SetHasNavigationBar(this, false);

                    etPhoneNumber.BackgroundColor = Color.Transparent;
                    break;
            }

            Navigation.PushModalAsync(new SplashScreenPagexaml());

            // show saved number
            object name = "";
            if (Application.Current.Properties.TryGetValue("phone_number", out name))
                etPhoneNumber.Text = (string)Application.Current.Properties["phone_number"];

        }

        private void btnGetCodeClicked(object sender, EventArgs e)
        {
            if (Constant.CHECK_FIELDS_ENTER)
                if (etPhoneNumber.Text.Length.Equals(12))
                {

                    Application.Current.Properties.Remove("phone_number");
                    Application.Current.Properties.Add("phone_number", etPhoneNumber.Text);

                    Navigation.PushAsync(new AddSmsCodePage());
                }
                else
                    DisplayAlert(null, AppResources.enter_correct_number, AppResources.ok);
            else
                Navigation.PushAsync(new AddSmsCodePage());

            //showNotification();
        }

        private void showNotification()
        {
            var notificator = DependencyService.Get<IToastNotificator>();
            var notificationOptions = new NotificationOptions()
            {
                Title = "Title",
                Description = "Description"
            };
            var result = notificator.Notify(notificationOptions);
        }
    }
}