﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace SportsmenCard
{
    public partial class SplashScreenPagexaml : ContentPage
    {
        public SplashScreenPagexaml()
        {
            InitializeComponent();
            autoClose();
        }

        private async void autoClose()
        {
            await Task.Delay(2500);
            await Navigation.PopModalAsync();
        }
    }

}