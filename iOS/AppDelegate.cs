﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Plugin.Toasts;

namespace SportsmenCard.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            // https://github.com/EgorBo/Toasts.Forms.Plugin
            // https://xamarinhelp.com/toasts-plugin-for-xamarin-and-windows/
            DependencyService.Register<ToastNotification>(); // Register your dependency
            ToastNotification.Init();
           
            LoadApplication(new App());
            //LoadApplication(new App());

            // set global tint color
            Boolean result = base.FinishedLaunching(app, options);
            UIApplication.SharedApplication.KeyWindow.TintColor = UIColor.Red;

            return result;







        }
    }
}
